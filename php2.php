
<!-- Crea una tabla de 4 columnas y 3 filas 
Ejecución:
                Tabla
        1	2	3	4
1	1-1	1-2	1-3	1-4
2	2-1	2-2	2-3	2-4
3	3-1	3-2	3-3	3-4-->


<?php
//tr fila
//td columna
$columnas = 4;
$filas    = 3;

echo "<table border=\"1\">";            // Abre la tabla con el borde uno

echo "<caption>Tabla</caption>";      // Crea la leyenda <caption> título

echo "<tbody>";                       // Abre el cuerpo de tabla <tbody>

echo "<tr>";                        // Abre la primera fila

echo "<th></th>";                 // Crea la primera celda <th> de la primera fila (sin número) 'th' para poner titulos de las cabeceras filas
    //titulo tabla horizontal
    for ($j = 1; $j <= $columnas; $j++) {      // Bucle 1 se ejecuta tantas veces como columnas tenga la tabla, (4 veces se ejecuta)
        
        echo "<th>$j</th>";           // Crea el título de la celda, que será el valor de 'j' que visualizará del 1 al 4
    }
    
echo "</tr>";                       // Cierra la primera fila
    //titulo tabla vertical
    for ($i = 1; $i <= $filas; $i++) {         // Bucle 2 (genera el resto de filas de la tabla, es decir 3)
        echo "<tr>";                    // Abre la fila
        echo "<th>$i</th>";           // Crea el título de la celda , que será el valor de 'i' que visualizará del 1 al 3
        
        //contenido de la tabla con los valores
        for ($j = 1; $j <= $columnas; $j++) {  // Bucle 3 se ejecuta tantas veces como columnas tenga la tabla, es decir, 4
            echo "<td>$i-$j</td>";     // Crea el resto de celdas <td> de cada fila con el contenido de la coordenada de la fila y la columna donde esta situada.
              }
        
    echo "</tr>";                   // Cierra la fila
    }

echo "</tbody>";                      // Cierra el cuerpo de tabla <tbody>
echo "</table>";                        // Cierra la tabla
?>
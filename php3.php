<!--Ejercicio foreach: Imprima los valores del vector asociativo siguiente usando la estructura de control
foreach:
Ejecución:
El elemento de indice 1 vale 90 
El elemento de indice 30 vale 7 
El elemento de indice e vale 99 
El elemento de indice hola vale 43 
El elemento de indice 0 vale Raquel -->

    <?php
        $vector = array();  // creamos un array llamado vector
        $vector[1]=90;  // en la posición nº uno (que es el indice) del vector añadimos el valor numérico 90
        $vector[30]=7;  // en la posición nº treinta del vector añadimos el valor numérico 7
        $vector['e']=99;  // en la posición 'e' del vector añadimos el valor numérico 99. Los vectores ordenan primero por numero y luego por letras por orden alfabético.
        $vector['hola']=43;  // en la posición 'hola' del vector añadimos el valor numérico 43. 
        $vector[0] = 'Raquel';  // en la posición 0 del vector añadimos el valor string 'Raquel'
        
        // foreach() es una función para recorrer los arrays. Mientras haya valores la función los devuelve.
       
         //El bucle asigna a la variable $indice la posicion del array del elemento actual que está recorriendo en ese momento, en la siguiente iteración devolverá el siguiente valor.
        //Y le asignamos a la variable $valor el valor que tiene en ese momento el array.  '=>' asigna un valor a un indice
        foreach ($vector as $indice => $valor){
            echo "El elemento de indice $indice vale $valor <br>";
        }
    ?>
    
